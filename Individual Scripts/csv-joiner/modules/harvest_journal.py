#!/usr/bin/env python3

"""A module that provides functions for manipulating the 'Harvest Journal' add-on
in Nav."""

from pathlib import Path
import time

import pandas as pd
import pyautogui

from modules import ht_pyautogui


# # # Globals / Logger # # #

# # Path # #

Data = Path("data")
Logs = Data / "logs"
Png = Data / "png"
Csv = Data / "csv"

# Data Tables
Strains = Csv / "Harvest Journal Strains.csv"

# PNG Files
Harvest_JournalPNG = str(Png / "harvestJournal.png")
Edit_Harvest_JournalPNG = str(Png / "editHarvestJournal.png")
Creation_DatePNG = str(Png / "creationDate.png")
Sort_DescendingPNG = str(Png / "sortDescending.png")
Creation_Date_SortedPNG = str(Png / "creationDateSorted.png")
Create_PackagePNG = str(Png / "createPackage.png")


def open_nav() -> None:
    """Uses PyAutoGUI to use keyboard shortcuts to open 'Microsoft NAV',
    using appropriate wait times."""
    time.sleep(1.0)
    # Start Menu won't open if super is pressed immediately
    pyautogui.run("k'win' w'Dynamics NAV' s0.7 k'enter'")
    # Wait 0.5 for Windows to catchup
    return None


def open() -> None:
    """Open 'Harvest Journal', a 'Microsoft NAV' addon, and snaps it to
    fullscreen.
    Assumes Nav is not already open."""
    open_nav()  # Wait for NAV to load and open Harvest Journal
    ht_pyautogui.wait_click(Harvest_JournalPNG)  # Open Harvest Journal
    ht_pyautogui.snap_fullscreen(ht_pyautogui.wait_for(Edit_Harvest_JournalPNG,
                                 Timeout=20,
                                 Timeout_Command=ht_pyautogui.unsnap_fullscreen))
    # Wait for Harvest Journal window title
    return None


def export() -> pd.DataFrame:
    """Exports current 'Harvest Journal' data into a pandas.DataFrame by
    copying the table to the clipboard, then reading the clipboard.
    Assumes 'Harvest Journal' is already open."""
    # Right-click to Organize by Date
    ht_pyautogui.wait_rclick(Creation_DatePNG)
    ht_pyautogui.wait_click(Sort_DescendingPNG)
    ht_pyautogui.wait_for(Creation_Date_SortedPNG)

    # Copying after highlighting all, loads the whole table after a
    # loading-bar pop-up
    pyautogui.hotkey("ctrl", 'a')  # Highlight whole table
    pyautogui.hotkey("ctrl", "shift", 'c')  # Copy highlighted cells
    time.sleep(20)  # Wait to load NAV to the clipboard

    # Get current batch names and waste values from clipboard
    return pd.read_clipboard(sep='\t', thousands=',')


def read_batch() -> str:
    """Reads and extracts batch from the clipboard using Pandas."""
    return str(pd.read_clipboard(sep='\t')["Harvest Batch No."].values[0])


def read_active_batch() -> str:
    """Uses PyAutoGUI to copy the active/highlighted row to the
    clipboard, then calls read_batch as its return values to read and
    extract the batch from the entry data."""
    pyautogui.hotkey("ctrl", "shift", 'c')  # Copy active
    return read_batch()
