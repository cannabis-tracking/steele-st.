#! /usr/bin/env python3

"""Creates a JSON file by continuing to accept a key and value by
as it can be used to convert Excel tables into JSON files."""

import json

# from tgs import Extensions as Ext


def main():
    Stop = (None, "")
    filename = input("Enter a name for the JSON file: ")
    if ".json" not in filename:
        if ".JSON" in filename:
            filename.replace(".JSON", ".json")

        else:
            filename += ".json"

    table = {}
    while True:
        key = input("Enter a key: ")
        if key in Stop:
            break

        value = input("Enter a value: ")
        if value in Stop:
            break

        table[key] = value

    with open(filename, 'w', encoding="utf-8") as json_file:
        json.dump(table, json_file, ensure_ascii=False, indent=4)

    print(f"File saved as {filename}. Exiting...")

    return


if __name__ == "__main__":
    main()
