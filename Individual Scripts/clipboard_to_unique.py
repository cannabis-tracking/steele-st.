#! /usr/bin/env python3

"""Takes current clipboard, containing a newline separated list, and
replaces it with a set of the original list."""

import pyperclip


def main() -> None:
    Blanks = (None, "", (), [])
    clipboard_string = pyperclip.paste()  # Get a newline separated string
    clipboard_list = clipboard_string.split('\n')
    # Break-up string by newline

    # Remove blank values
    for index, string in enumerate(clipboard_list):
        if string in Blanks:
            del clipboard_list[index]
    clipboard_set = set(clipboard_list)
    pyperclip.copy('\n'.join(clipboard_set))

    return None


if __name__ == "__main__":
    main()
