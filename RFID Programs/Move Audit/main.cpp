// Raspi Program for Move Audits

#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <ctime>
#include <set>

int main(int argc, char** argv)
{
	const char DateSep = '/';
	const char Sep = ',';

	auto t = time(0);
	auto tm = *localtime(&t);

	std::ostringstream oss;
	oss << std::put_time(&tm, "%m/%d/%Y");
	const std::string Date = oss.str();

	int Tag_Count;
	std::string room;
	std::string rfid;
	std::set<std::string> tags;

	std::cout << "Enter the number of tags being scanned: ";
	std::cin >> Tag_Count;
	std::cout << "Enter the room the tags are being place in: ";
	std::cin >> room;
	std::cin.ignore(10000, '\n');

	for (int i = 0; i < room.size(); i++)
	{
		room[i] = std::toupper(room[i]);
	}

	while(tags.size() < Tag_Count)
	{
		std::cout << "Unique Tags: " << tags.size();

		for (int index = 0; index < 7; index++)
		{
			std::cout << '\n';
		}

		std::cout << "Scan a tag: ";
		std::getline(std::cin, rfid);
		std::cout << '\n';

		// add to set
		tags.insert(rfid);
	}

	std::cout << "Saving " << Tag_Count << " tags to a text file called "
		<< room << ".csv";

	// WRITE TO FILE AND SAVE/CLOSE
	std::ofstream tagfile(room + ".csv");

	if (tagfile.is_open())
	{
		std::set<std::string>::iterator ptr;
		for (ptr = tags.begin(); ptr != tags.end(); ptr++)
		{
			tagfile << *ptr << Sep << Sep << "Flowering" << Sep
				<< room << Sep << Date << "\r";
		}
		tagfile << '\n';
		tagfile.close();
	}
	else // unable to open file
	{
		std::cout << "Error writing the file...\n"
			<< "Exiting...";

		return -1;
	}

	return 0;
}
