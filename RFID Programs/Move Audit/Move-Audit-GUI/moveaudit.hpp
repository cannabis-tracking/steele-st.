#ifndef MOVEAUDIT_HPP
#define MOVEAUDIT_HPP

#include <QMainWindow>

namespace Ui
{
  class MoveAudit;
}

class MoveAudit : public QMainWindow
{
  Q_OBJECT

public:
  explicit MoveAudit(QWidget *parent = 0);
  ~MoveAudit();

private:
  Ui::MoveAudit *ui;

/*
private slots:
  void numPressed();
  void togglePressed();
  void backspacePressed();
  void submitEnterPressed();
  void comboPressed();
*/
};

#endif // MOVEAUDIT_HPP
