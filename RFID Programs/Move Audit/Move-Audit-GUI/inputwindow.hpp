#ifndef INPUTWINDOW_HPP
#define INPUTWINDOW_HPP

#include <QDialog>

namespace Ui {
  class InputWindow;
}

class InputWindow : public QDialog
{
  Q_OBJECT

public:
  explicit InputWindow(QWidget *parent = 0);
  ~InputWindow();

private:
  Ui::InputWindow *ui;
};

#endif // INPUTWINDOW_HPP
